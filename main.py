import pyb
import utime
from encoder import Encoder
from controller import Controller
from Motor import MotorDriver 
from sensor import Sensor

# @file main.py
# File contains cooperative multitasking code for the Turn Signal Machine
#
# @author Felipe Dejesus
# @date 11 June 2020


##
# @breif Initialization code for motors, encorders, controllers, and the IMU
# @details The pyboard is initialized for the correct motor, encoder, and sensor
#          pin addresses. Returns global Values for drivers
def INIT():
    
    print('INITIALZING SYSTEMS')
    # init motor 1 -- Motor 1 runs off pins B4 and B5, timer 3
    pin_EN_1 =  pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1_1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2_1 = pyb.Pin(pyb.Pin.cpu.B5)
    tim_m_1 = pyb.Timer(3,freq = 20000)
    global MOT_1
    MOT_1 = MotorDriver(pin_EN_1, pin_IN1_1, pin_IN2_1, tim_m_1)   
    
    # init encoder 1 -- Encoder 1 runs off pins B6 and B7, timer 4
    PinB6 = pyb.Pin (pyb.Pin.board.PB6, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF2_TIM4)
    PinB7 = pyb.Pin (pyb.Pin.board.PB7, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF2_TIM4)
    tim_e_1 = pyb.Timer(4, prescaler = 0, period = 0xffff)
    global ENC_1
    ENC_1 = Encoder(PinB6,PinB7,tim_e_1)
         
    # init Controller 1 -- Controller 1 only needs proper ENC 1 and MOT 1
    Kp_1 = .09
    set_point_1 = 0
    global CONT_1
    CONT_1 = Controller(Kp_1, set_point_1, ENC_1, MOT_1)
            
    # init motor 2 -- Motor 2 runs off 
    pin_EN_2 =  pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
    pin_IN1_2 = pyb.Pin(pyb.Pin.cpu.A0)
    pin_IN2_2 = pyb.Pin(pyb.Pin.cpu.A1)
    tim_m_2 = pyb.Timer(5,freq = 20000)
    global MOT_2
    MOT_2 = MotorDriver(pin_EN_2, pin_IN1_2, pin_IN2_2, tim_m_2)
         
    # init encoder 2 -- Encoder 2 runs on pins C6 and C7, timer 4
    PinC6 = pyb.Pin (pyb.Pin.board.PC6, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF3_TIM8)
    PinC7 = pyb.Pin (pyb.Pin.board.PC7, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF3_TIM8)
    tim_e_2 = pyb.Timer(8, prescaler = 0, period = 0xffff)
    global ENC_2
    ENC_2 = Encoder(PinC6,PinC7,tim_e_2)     
       
    # init Controller 2 -- Controller 2 only needs ENC_2 and MOT_2
    Kp_2 = .09
    set_point_2 = 0
    global CONT_2
    CONT_2 = Controller(Kp_2, set_point_2, ENC_2, MOT_2)     
    
    # init IMU -- IMU connects to SCL and SDA ports on Board
    i2c = pyb.I2C(1,pyb.I2C.MASTER)
    global SENS 
    SENS = Sensor(i2c,40)
    
    # init turn counters
    global R
    global L
    global R_turn
    global L_turn
    R_turn = 0
    L_turn = 0
    R = 0
    L = 0
    print ('ALL SYSTEMS GO!')
    
## Hub state for machine. Constantly polls sensor for position. Sets 
# global booleans for left and right flags based on position
def HUB(SENS):
    
    pos = SENS.get_pos()
    global R_FLG
    global L_FLG 
    global R_turn
    global L_turn
    if pos[2] >= 10:
        R_FLG = 1
        L_FLG = 0
        R_turn = 1
    elif pos[2] <= -10:
        R_FLG = 0
        L_FLG = 1
        L_turn = 1
    else:
        R_FLG = 0
        L_FLG = 0


## FSM task for state 2. Moves Right Flag on and off according to boolean
def RIGHT(R_FLG): 
    if R_FLG == 1:                # Moves Flag For Right Signal
        set_point_1 = -300
        CONT_1.update(set_point_1)
    else:                         # Moves Flag Back to Center
        set_point_1 = 0
        CONT_1.update(set_point_1)

## FSM Task for state 3. Moves Left Flag on and off according to boolean
def LEFT(L_FLG):   
    if L_FLG == 1:                # Moves Flag for Left Signal
        set_point_2 = 300
        CONT_2.update(set_point_2)
    else:                         # Moves Flag Back to Center
        set_point_2 = 0
        CONT_2.update(set_point_2)

## Task that tracks Right and Left turns based on booleans. When a turn is comleted
# a message is printed with the number of turns up to that point.
def TURNS(R_FLG,L_FLG):
    global R
    global L
    global R_turn
    global L_turn
    if R_turn == 1 and R_FLG == 0:
        R_turn = 0
        R = R+1
        print('Right Turn Completed. {} total right turns'.format(R))
    elif L_turn == 1 and L_FLG == 0:
        L_turn = 0
        L = L+1
        print('Left Turn Completed. {} total left turns'.format(L))
    else:
        pass
       
if __name__ == '__main__' : 
    # Initialize the PyBoard for the encoders, motors, and the IMU
    INIT()
    
    ## @brief   Task loop rate in uS
        #  @details A Python integer that represents the nominal number of microseconds
        #           between iterations of the task loop.
    interval = 100000
        
        ## @brief   The timestamp of the next iteration of the task loop
        #  @details A value from utime.ticks_us() specifying the timestamp for the next
        #           iteration of the task loop.
        #
        #           This value is updated every time the task loop runs to schedule the
        #           next iteration of the task loop.
    next_time = utime.ticks_add(utime.ticks_us(), interval)
        
        ## @brief   The timestamp associated with the current iteration of the task
        #           loop
        #  @details A value from utime.ticks_us() updated every time the task loop 
        #           checks to see if it needs to run. As soon as this timestamp exceeds
        #           next_time the task loop runs an iteration.
    cur_time = utime.ticks_us()
    while True:
        cur_time = utime.ticks_us()
        if utime.ticks_diff(cur_time, next_time) > 0:
            next_time = utime.ticks_add(next_time, interval)
            HUB(SENS)
            RIGHT(R_FLG)
            LEFT(L_FLG)
            TURNS(R_FLG,L_FLG)


