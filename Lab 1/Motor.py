import pyb
# @file Motor.py
# File contains motor driver and a small UI for testing
#
# @date Sun Apr 19 18:21:27 2020
#
# @author: Felipe DeJesus
#
# @package Motor Driver
# Motor Dirver Creates a class given the inputs valid inputs for
# (EN_Pin,IN1_Pin,IN2_pin,Timer)
# the driver will create the necessary object and contains 3 commands
# @author Felipe DeJesus
# @date 20 April 2020
#
#
## This class implements a motor driver for the
# ME405 board. 
class MotorDriver:

 ## Constructor for motor driver.
 # Creates a motor driver by initializing GPIO
 # pins and turning the motor off for safety.
 # @param EN_pin A pyb.Pin object to use as the enable pin.
 # @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
 # @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
 # @param timer A pyb.Timer object to use for PWM generation on IN1_pin
 # and IN2_pin. 
 def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
    
    print ('Creating a motor driver')
    self.En_pin = EN_pin
    self.IN1_pin = IN1_pin 
    self.IN2_pin = IN2_pin
    self.timer = timer
    self.PWM1 = self.timer.channel(1,pyb.Timer.PWM, pin = self.IN1_pin)    
    self.PWM2 = self.timer.channel(2,pyb.Timer.PWM, pin = self.IN2_pin)      
        
 ## Enables Motor for operation. Sets motor to stop for safety
 # Returns Enable message     
 def enable (self):
    
    print ('Enabling Motor')
    self.En_pin.high ()
    self.PWM1.pulse_width_percent(0)
    self.PWM2.pulse_width_percent(0)
    
 ## Disables Motor for operation. Returns Disable Message
 def disable (self):

    print ('Disabling Motor')
    self.En_pin.low ()
   
 ## This method sets the duty cycle to be sent
 # to the motor to the given level. Positive values
 # cause effort in one direction, negative values
 # in the opposite direction.
 # @param duty A signed integer holding the duty
 # cycle of the PWM signal sent to the motor.
 # signed integer must bne between -100 and 100        
 def set_duty (self, duty):
    
    self.duty = duty
    self.PWM1.pulse_width_percent(0)
    self.PWM2.pulse_width_percent(0) 
     
    
    if duty >=  0:
     self.PWM1.pulse_width_percent(self.duty)
     self.PWM2.pulse_width_percent(0)
     
     
    else:
     self.PWM2.pulse_width_percent(abs(self.duty))
     self.PWM1.pulse_width_percent(0)
    
        
        
if __name__ == '__main__':
     import pyb
     
     # Create the pin objects used for interfacing with the motor driver
     pin_EN =  pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
     pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4);
     pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5);
     
     
     # Create the timer object used for PWM generation
     tim = pyb.Timer(3,freq = 20000);
    
    
     # Create a motor object passing in the pins and timer
     moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
     
     
     # This part controls the user interface
     while True:
          action = input('Enter prompt to [E]nable' 
                         '[D]isable or [S]et Duty Cycle:')
          
          if action == 'E':  # Enable the Motor
               moe.enable()
               continue
       
          elif action == 'D': # Diasble the Motor
               moe.disable()
               continue
           
          elif action == 'S': # Set Duty Cylce
               try:
                   eff = int(input('Please enter a duty cycle(-100 to 100): '))
                   assert(100>eff>-100)
               except:
                   print('please enter an integer between -100 and 100')
                   continue
               
               moe.set_duty(eff)
               print('duty cycle set to {}'.format(eff))
               continue
                
          else:
              print('Please enter a valid command')
              continue