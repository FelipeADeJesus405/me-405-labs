# @file sensor.py
#
# Sensor creates a driver for reading positional data from the sensor.
#
# Sensor is based on the Bosch BNO055. The class accepts inputs for IIC path
# and device address, and can return calibration points, euler angles, and
# angular velocity
#
# @author Felipe DeJesus
#
# @date 18 May 2020
#
# @package Sensor
# Sensor is based on the Bosch BNO055. The class accepts inputs for IIC path
# and device address, and can return calibration points, euler angles, and
# angular velocity
#
# @author Felipe DeJesus
# @date 18 May 2020

#
## This class implements a sensor reader for the
# ME405 board. 
class Sensor:
  
 ## Constructor for Sensor.
 # 
 # @param i2c properly configured IIC MASTER for the sensor
 #   
 # @param addr address in IIC for the device you wish to read
 #   
 # @author Felipe DeJesus
 # @date  18 May 2020vn  


 def __init__ (self, i2c,addr):
     self.i2c = i2c
     self.addr = addr
     self.i2c.mem_write(0b1100,self.addr,0x3D)
     # set sensor mode
     self.i2c.mem_write(0b1000,self.addr,0x61)
     self.i2c.mem_write(0b1000,self.addr,0x63)
     self.i2c.mem_write(0b1000,self.addr,0x65)
     # calibrate on init
     self.i2c.mem_write(0b0000,self.addr,0x3B)
     #  set units
     
 ## This method allows the user to set the mode of the sensor. Enter [1] to 
 # put the sensor into nine degree of freedom fusion mode. Enter [2] to
 # disable the sensor. All other inputs do nothing and return an error message
 
 def set_mode (self, u_set):
     self.uset = u_set
     # 3D is the opr mode register
     if self.u_set == 1:
         self.i2c.mem_write(0b1100,self.addr,0x3D)
         print('Sensor Enabled')
         # Writes NDOF to operation register
         
     elif self.u_set == 2:
         self.i2c.mem_write(0b000,self.addr,0x3D)
         print ('Sensor Diasabled')
         # Writes CONFIGMODE to operation register
         
     else:
         print('Invalid Mode')
         
 ##  This method allows the user to check the current calibration of the sensor
 # the caliibration is read from the gyro and accelerometer offset registers
 def check_cal (self):
     self.X = self.i2c.mem_read(2,self.addr,0x61)
     self.X = ((self.X[1]<<8)+self.X[0])/16
     if self.X >= 2048:
         self.X -= 4096
     else:
         pass
     # NOTE: 2048 and 4096 are 32768 and 65535 divided by 16
        
     self.Y = self.i2c.mem_read(2,self.addr,0x63)
     self.Y = ((self.Y[1]<<8)+self.Y[0])/16
     if self.Y >= 2048:
         self.Y -= 4096
     else:
         pass
     
     self.Z = self.i2c.mem_read(2,self.addr,0x65)
     self.Z = ((self.Z[1]<<8)+self.Z[0])/16
     if self.Z >= 2048:
         self.Z -= 4096
     else:
         pass
    
     self.X_1 = self.i2c.mem_read(2,self.addr,0x55)
     self.X_1 = ((self.X_1[1]<<8)+self.X_1[0])/16
     if self.X_1 >= 2048:
         self.X_1 -= 4096
     else:
         pass
     
     self.Y_1 = self.i2c.mem_read(2,self.addr,0x57)
     self.Y_1 = ((self.Y_1[1]<<8)+self.Y_1[0])/16
     if self.Y_1 >= 2048:
         self.Y_1 -= 4096
     else:
         pass
     
     self.Z_1 = self.i2c.mem_read(2,self.addr,0x59)
     self.Z_1 = ((self.Z_1[1]<<8)+self.Z_1[0])/16
     if self.Z_1 >= 2048:
         self.Z_1 -= 4096
     else:
         pass
    
    
     print('current Euler offsets X:{} Y:{} Z:{}'.format(self.X,self.Y,self.Z))
     print('current Acc Offsets X:{} Y:{} Z:{}'.format(self.X_1,self.Y_1, self.Z_1))
     
 ##  This method returns the yaw, roll, and pitch positions of the sensor as a tuple.
 # The euler angles are read from the roll, pitch, and yaw registers.
 # only valid in fusion mode. yaw: [0 360] roll:[-90 90] pitch:[-180 180].
 # Units of Degrees [D]
 def get_pos(self):
     #yaw
     self.y = self.i2c.mem_read(2,40,0x1A)
     self.yaw = int(((self.y[1]<<8)+self.y[0])/16)
     
     #roll
     self.r = self.i2c.mem_read(2,40,0x1C)
     self.roll = int(((self.r[1]<<8)+self.r[0])/16)
     
     if self.roll >= 2048:
         self.roll -= 4096
     else:
         pass
     
     #pitch
     self.p = self.i2c.mem_read(2,40,0x1E)
     self.pitch = int(((self.p[1]<<8)+self.p[0])/16)
     
     if self.pitch >= 2048:
         self.pitch -= 4096
     else:
         pass
     
     #Tuple
     return (self.yaw,self.roll,self.pitch)
     
     
 ##  This method returns the x,y, and z angular velocities a tuple.
 # the velocity are read from the gyro x,y, and z registers.
 # Units of Degrees per Second [Dps]
 def get_velocity(self):
     #acc x
     self.x_2 = self.i2c.mem_read(2,40,0x14)
     self.x_acc = int(((self.x_2[1]<<8)+self.x_2[0])/16)
     
     if self.x_acc >= 2048:
         self.x_acc -= 4096
     else:
         pass
     
     #acc y
     self.y_2 = self.i2c.mem_read(2,40,0x16)
     self.y_acc = int(((self.y_2[1]<<8)+self.y_2[0])/16)
     
     if self.y_acc >= 2048:
         self.y_acc -= 4096
     else:
         pass
     
     #acc z
     self.z_2 = self.i2c.mem_read(2,40,0x18)
     self.z_acc = int(((self.z_2[1]<<8)+self.z_2[0])/16)
     
     if self.z_acc >= 2048:
         self.z_acc -= 4096
     else:
         pass
     
     #Tuple
     return (self.x_acc,self.y_acc,self.z_acc)
    
if __name__ == '__main__':
    import pyb
    import utime
    i2c = pyb.I2C(1,pyb.I2C.MASTER)
    sens = Sensor(i2c,40)
    
    
    for i in range (1,100):
        n = sens.get_pos()
        print('data {}'.format(n))
        utime.sleep_ms(100)
        

 
    

