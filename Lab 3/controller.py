from motor import MotorDriver
from encoder import Encoder 
# @file controller.py
#
# Controller creates a closed loop position control for our motor.
#
# Controller takes inputs for set point and Kp. Controler relies on motor.py
# and encoder.py
#
# @author Felipe DeJesus
#
# @date 11 May 2020
#
# @package Controller
# Controller creates a class to for closed loop position control.
# The Controller utilizes the motor driver and encoder to function.
#
# @author Felipe DeJesus
# @date 11 May 2020

#
## This class implements a position controller for the
# ME405 board. 
class Controller:
  
 ## Constructor for Controller.
 # 
 # @param Kp Proportional gain that the error signal is mulitplied by. Must be
 # tuned to function well
 #   
 # @param Set_point User input on where they want the motor to go. 
 #
 # @param ENC Encoder input for the controller so it knows where to read
 # position from
 #   
 # @param MOT Motor input for the controller so it knows where to send the 
 # output signal
 #   
 # @author Felipe DeJesus
 # @date  11 May 2020


 def __init__ (self, Kp, ENC, MOT):
     self.Kp = Kp                            # Init user inputs in the class
     self.ENC = ENC
     self.MOT = MOT 
     self.e = 0                              # clears working variables
     self.f = 0
     self.s = 0
     self.MOT.enable()
     
 ## Updates the controller with the current feedback controll. Must be called
 # continuously to function properly.    
 
 def update (self,set_point):
    self.set_point = set_point
    self.ENC.update()
    self.f = self.ENC.get_position()         # calls encoder to get position
    self.e = self.set_point - self.f         # Finds error signal
    self.s = self.Kp * self.e                # Multiplies error by Kp 
    self.MOT.set_duty(self.s)                # Calls motor to update pwm
