## @file mainpage.py
#  main page for ME 405 student Felipe DeJesus
#
# @mainpage
#
# @section sec_turn Turn Signal Machine
# The Turn Signal Machine is the culmination of my progress in the class. This
# machine uses the IMU, Motors, and Encoders to operate a machine that automatically
# lowers a turn signal flag while riding a bike. Further details can be seen
# in the Project page.
#
# Link: https://bitbucket.org/FelipeADeJesus405/me-405-labs/src/master/main.py
#
# @section sec_motor Motor Driver
# Motor Driver is a class that creates a motor driver
# this motor Driver can Enable and Disbale the Motor. Motor driver was tested
# for forwards and backwards travel.
# It can also set the Duty Cycle.
# No known bugs.
# location: https://bitbucket.org/FelipeADeJesus405/me-405-labs/src/master/Lab%201/
#
# @section sec_main Main Loop
# Main Loop contains a nice little UI for enable, disable and duty cycle set
# and returns error messages for invalid inputs
# location: currently paried with Motor Driver
#
#
# @section sec_encoder Encoder
# Encoder is a class that creates a driver for the Encoder. This class can
# show updated encoder position, return the most recent position of the 
# encoder, and show delta's between the 2 most recent position updates.
# ecoder was tested for forward and backward encoder movement, even across
# the zero and 65535 boundaries. Was also tested with two concurrent encoders.
# Limitiations: pin and timer inputs must be compatible.
# No known bugs.
# location: https://bitbucket.org/FelipeADeJesus405/me-405-labs/src/master/Lab%202/
#
#
# @section sec_controller Controller
# Controller is a class that creates a proportional only closed loop feedback
# controller for the motor. The controller relies on a properly set up motor
# and encoder to function properly. See the testing page for detailed information
# on tuning and testing.
# Limitations: cannot go past -65535 due to encoder limitations
# Bugs:
# location: https://bitbucket.org/FelipeADeJesus405/me-405-labs/src/master/Lab%203/
#
#
# @section sec_sensor Sensor
# Sensor is a class that allow us to read and write data to the BNO055 IMU.
# The user can read sensor orientation in the form of Euler angles [degrees], 
# and Angular Acceleration about the x,y, and z axes [degrees/sec].
# The controller must be enabled in fusion mode to function properly and connect
# to the SCL/SDA ports of B8  and B9 on the Nucleo Board. No know bugs
# location: https://bitbucket.org/FelipeADeJesus405/me-405-labs/src/master/Lab%204/
#
#
# @date 11 May 2020
# @author Felipe DeJesus
#
# @page sens_page Sensor Documentation
#
# 
# To function properly the BNO055 IMU must be initialized in fusion mode
# this can be accomlished by using the set_mode command and entering a [1].
# The sensor can be disabled with same command by entering [2]
#
#
# The command check_cal return the calibration points for the angular velocity
# and euler angle data. The units for these points are [degree/sec] and [deg] 
# respectivley. This data is returned as a string.
#
#
# The command get_pos returns the euler angles of the sensor in degrees. The 
# data is returned as a tuple in the order: YAW, ROLL, PITCH.
#
#
# The command get_velocity returns the angular velocity of the sensor in 
# degrees/sec. The data is returned as a tuple in the order: X,Y,Z
# 
# Video shows constant polling of get_position method. 
# Video Link: https://youtu.be/lknjyWMza8o
#
# @author Felipe DeJesus
# @date 19 May 2020
#
#
# @page test_page Testing 
#
# 
#
# Testing was done in thonny and matlab. The crieteria I was looking for 
# was fast responese time with minimal overshoot.
#
# I ran the tests with 3 values for Kp.
# in each test motor ran from zero to 1000 encoder ticks. The first test was 
# kp=.25 which resulted in a large overshoot in the matlab plots. From here I
# decreased kp to .1 which gave asmaller overshoot without oscillations.
# I further decreased Kp to .085 to acheive no overshoot with an
# acceptable response time. 
#
# Video Link: https://youtu.be/uGSYotmFgpQ
# @image html two.png
# @image html one.png
# @image html eight.png
# 
# @date 11 May 2020
# @author: Felipe DeJesus
#
#
# @page prop_page Project
#
# @section sec_pron Problem
# Often while riding my bike to school I forget to signal my turns. This can 
# be very problematic for myself and cars around me. For the project
# the problem I seek to solve is tomake a hands free way for me to signal bike
# turns on my commute. Additionally I would like to track the number of turns 
# I take on my ride.
#
#
# @section sec_req Requirements
# My project will use two actuators to move signal flags when I turn the bike.
# The system will use the IMU to telll when I lean my bike for a turn and when
# I come back to center to ride straight again. Additionally I will need to use
# the encoders to tell how far I want my signal flags to move.
#
# The new feature will be a tracker to log the turns I make on my ride.
#
# @section sec_fab Fabrication 
# I think this project will be fairly easy to fabricate. I will just have to
# figure out how to mount the nucelo to my bike. Likely under the seat. And then
# create flags that attach to the motors provided in the kit.
#
# @section sec_risks Risks
# I think the biggest risk with this project will be if I fell on the bike and
# hit the flags. I will wear a helmet and make the flags some nice material that
# wont impale me in an accident.
#
# @section sec_time Timeline
# 1 week to get the imu comminucating with the motors. 1 week to fabriocate the
# physical system. 1 week to find any bugs.
#
# @section sec_FSM Finite State Machines
# I used the following simple finite state macine diagram to create the project
#
# @image html FSM.jpg width=400px
#
# @section sec_Func Functionality 
# The Turn Signal Machine can be installed on a bike (not wirelessly at the moment)
# and will automatically lower a left or right turn signal flag when the user 
# leans into a turn (astute observers will note that the law requires turn signals
# before turning). Additionally when the Turn Signal Machine is connected to a 
# computer it will log and announce the number of right and left turns the user 
# makes
#
# @section sec_work Inner Workings
# The Turn Signal Machine uses a few tasks that I have implemented as Finite 
# State Machines. The controller works by continuously polling and sending signals
# to the motors so the machine must be run in a constant loop. Flag control and
# turns tracking is done by booleans. I wasnt sure how sophisticated to get with
# the design and I also wanted to challenge myself to figure as much of the program
# out on my own as possible. I'm sure it could be optimized but this is my crack
# at a simple Turn Signal Machine
#
# @section sec_testing Testing
# The Turn Signal Machine works exactly as expected. The flags lower when the IMU
# is tilted, and turns are tracked when connected to a computer. If I were to 
# take this project even further I would streamline the turn signal flags, and
# condense the wiring so the machine could be wireless. The video shows the functionality
# of the machine with a bike in my bedroom!
#
# Since I didnt have much space I had to make the IMU extra sensitive to tilt, resulting
# in slight jumpiness when returning to center. Also it was set up on the bread board
# so I had to be careful when moving stuff around.
#
# Video Link: https://www.youtube.com/watch?v=qC2fh1Syd5I
#
if __name__ == '__main__':
     # import functions 
     import pyb
     import utime
     from encoder import Encoder
     from controller import Controller
     from motor import MotorDriver   
     
     
     # init motor 
     pin_EN =  pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
     pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
     pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
     tim = pyb.Timer(3,freq = 20000)
     moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
     
     # init encoder
     PinB6 = pyb.Pin (pyb.Pin.board.PB6, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF2_TIM4)
     PinB7 = pyb.Pin (pyb.Pin.board.PB7, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF2_TIM4)
     tim_1 = pyb.Timer(4, prescaler = 0, period = 0xffff)
     ENC_1 = Encoder(PinB6,PinB7,tim_1)
     
     # init Controller
     Kp = .09
     set_point = int(input ('please enter a value for set point:'))
     control = Controller(Kp, set_point, ENC_1, moe)
     
     #init list
     time = []
     pos = []
     
     for i in range (1,275):
        utime.sleep_ms(10)
        control.update()
        ENC_1.update()
        time.insert(i, utime.ticks_ms())
        pos.insert (i, ENC_1.get_position())
     print ('run finished')
            
