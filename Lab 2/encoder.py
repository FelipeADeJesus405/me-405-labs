import pyb
# @file encoder.py
#
# Encoder allows position to be read from the motor encoder.
#
# Encoder class takes inputs for matching pin/timer pairs and allows
# the user to update and read values from the encoder
#
# @author Felipe DeJesus
#
# @date 1 May 2020
#
# @package Encoder
# Encoder Creates a class given the inputs valid inputs for
# the driver will create the necessary object and contains 3 commands
# @author Felipe DeJesus
# @date 1 May 2020

#
## This class implements an encoder driver for the
# ME405 board. 
class Encoder:
    
 ## Constructor for Encoder.
 # Creates an encoder driver given appropriate pin/timer combo
 # pins and setting up the timers
 # @param Pin_A  An alternate fcn pin object used for the A phase of the encoder 
 # @param Pin_B  An alternate fcn pin object used for the B phase of the encoder
 # @param Timer  A timer object used to read the encoder. Pin alternate fcn must
 # match the timer beingn used
 # @author Felipe DeJesus
 # @date  1 May 2020


 def __init__ (self, Pin_A, Pin_B, timer):
         
    print ('Initializing Encoder')
    self.Pin_A = Pin_A     
    self.Pin_B = Pin_B 
    self.timer = timer   
    self.Enc = self.timer.channel(1, pyb.Timer.ENC_AB)
    self.pos = 0
    self.new = 0
    self.old = 0
    self.delta = 0
    
    
 ## Updates the encoder with the current timer counter. Also updates the values
 #  for delta and position. Method contains logic to handle over and underflow
 #  update must be used to get new values for position and delta
 
 def update (self):
    print ('Updated')
    self.new = self.timer.counter()
    self.delta = self.new-self.old
    
    if self.delta >= 32768:
        self.delta = self.delta - 65535
        self.pos = self.old + self.delta
        self.old = self.old + self.delta
        # prevents underflow errors
        
        
    elif self.delta <=-32678:
        self.delta = self.delta + 65535
        self.pos = self.old + self.delta
        self.old = self.old + self.delta
        #prevents overflow errors
        
        
    else:
        self.pos = self.old + self.delta
        self.old = self.new
    
    
    
    
 ## Returns current position in a print string. Update must be called to 
 #  update position value
 def get_position (self):
     print ('Most recent updated position is: {}'.format(self.pos))
     
     
 ## Sets position to user specified input. Also updates the Encoder driver
 # for get position and get delta
 # @param pos_set A signed integer less than 0xFFFF for the user to set
 # the encoder counter
 def set_position (self, pos_set):
    self.pos_set = pos_set
    print (' Position has been updated to {}'. format(self.pos_set))
    self.timer.counter(self.pos_set)
    self.pos = self.pos_set
    self.new = self.timer.counter()
    self.delta = self.new-self.old
    self.old = self.new
    
 ## Returns difference between the two most recent position updates. Update must 
 #  be called to update the delta value.
         
 def get_delta (self):
     print ('most recent delta is: {}'.format(self.delta))
     
    
if __name__ == '__main__':
     import pyb
     
     PinB6 = pyb.Pin (pyb.Pin.board.PB6, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF2_TIM4)
     PinB7 = pyb.Pin (pyb.Pin.board.PB7, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF2_TIM4)
     PinC6 = pyb.Pin (pyb.Pin.board.PC6, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF3_TIM8)
     PinC7 = pyb.Pin (pyb.Pin.board.PC7, mode=pyb.Pin.AF_PP, af=pyb.Pin.AF3_TIM8)
     # Creates Pin objects for both encoders using the appropriate alternate function
     
     tim_1 = pyb.Timer(4, prescaler = 0, period = 0xffff)
     # PB6 = Tim 4 ch1
     # PB7 = Tim 4 ch2
     
     tim_2 = pyb.Timer(8, prescaler = 0, period = 0xffff)  
     # PC6 = Tim 3/8 ch1
     # PC7 = tim 3/8 ch2
     
     ENC_1 = Encoder(PinB6,PinB7,tim_1)
     ENC_2 = Encoder(PinC6,PinC7,tim_2)
