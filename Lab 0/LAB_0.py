# -*- coding: utf-8 -*-
## @file Lab_0.py
# @mainpage
# @section sec_fib Fibonacci
# The fibonacci Sub routine returns fibonacci numbers given an index.
#
# @section Main Loop
# main loop creates a UI. Accepts valid index and returns error messages.
#
# @author Felipe DeJesus
#
# @date April, 4 2020



"""
Created on Mon Apr  6 11:49:09 2020
This Program prompt the user for a fibonacci index, then calculates a 
fibonacci number based on that index. The program displays error messages
when input is not a positive integer, and exits out when the user enters 
an index of 999
@author: Felipe DeJesus
""" 
from math import sqrt
from sys import exit

def fib(idx):
    '''
    Edgecombe, T(2017). Ambu,A (2009) How To Write the Fibonacci Sequence?.
    https://stackoverflow.com/questions/494594/how-to-write-the-fibonacci-sequence
    
    This Method Calculated Fibonnaci Numbers given a specified index.
    @param idx An integer specifying the index of the desired Fibonacci Number.
    '''
    if idx == 999:
        print ('exiting')
        exit()
    # If 999 is entered by the user, program will write message and exit
                     
    else:
        print ('calculating Fibonnaci number at '
           'index n = {}'.format(idx))
        s = (((1+sqrt(5))**idx)-(1-sqrt(5))**idx)/((2**idx)*sqrt(5))
        return int(s)
    # if a valid index is given, fibonacci number will be returned   
    # and calculations message will be written (int function removes float)



if __name__ == '__main__':
    while True:
        try:
            idx =int(input('Please Enter an index for a Fibonacci Number: '))
            assert(idx>0)
        # BEAST, A(2016). How can I make user inputs a positive integer in Python?
        # https://www.quora.com/How-can-I-make-sure-the-user-inputs-a-positive-integer-in-Python
            
        # Checks user input to make sure its an integer
        # If its not an integer goes to "Check" and displays error msg
        # Assert command ensures there are no values less than zero

        except:
            print('Please Enter a Positive Integer')
            continue
        # Pints error message and then starts sequence over again
        
        print(fib(idx))
        # If given a valid index prints the results from the fib Subroutine